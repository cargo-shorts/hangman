// Copyright (C) 2020 Fatcat560/Mario Spies
// Copyright (C) 2020-2 Arc676/Alessandro Vinciguerra <alesvinciguerra@gmail.com>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation (version 3)

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use hangman::hangman::Hangman;
use std::path::Path;

fn main() {
    println!("Hangman!");

    let guess_protection = match read_input("Protect against duplicate guesses? [y/N]: ").as_str() {
        "y" | "Y" => true,
        _ => false,
    };

    // Initialize Hangman game using provided word list or asking for user input
    let hangman_res = match std::env::args().nth(1) {
        Some(path) => Hangman::new_from_word_list(Path::new(&path), 8),
        None => Hangman::new(read_input("Enter the secret: "), 8),
    };
    // Check that the Hangman game was successfully initialized
    let mut hangman = match hangman_res {
        Ok(x) => x,
        Err(y) => {
            println!("Couldn't start a hangman game: {}", y);
            return;
        }
    };
    while hangman.game_ongoing() {
        println!("{}", hangman);
        let user_guess = read_input("Enter your guess: ");
        if guess_protection && hangman.has_guessed(&user_guess) {
            println!("You already guessed this.");
            continue;
        }
        let guess = hangman.handle_guess(user_guess);
        if let Err(x) = guess {
            println!("{}", x);
            continue;
        }
        if guess.unwrap() {
            println!("Correct!");
        } else {
            println!("Wrong!");
        }
    }
    println!(
        "\nGame over!\n\nSecret was:\t\t{}\nFinal status:\t{}",
        hangman.get_secret(),
        hangman.get_current_status()
    );
}

fn read_input<T>(prompt: T) -> String
where
    T: AsRef<str>,
{
    println!("{}", prompt.as_ref());
    let stdin = std::io::stdin();
    let mut buff = String::new();
    while let Err(_) = stdin.read_line(&mut buff) {
        println!("An error occurred, try again: ");
    }
    String::from(buff.trim())
}

# Hangman

A Rust crate for the popular game Hangman

# Licensing

Project available under GPLv3. The word list `2of12.txt` contains American English words and was compiled by Alan Beale and obtained from [SCOWL](http://wordlist.aspell.net/12dicts/). The list is released into the public domain.
